import React from 'react';
import { Route, Switch} from 'react-router-dom';
import HomePage from "../../containers/HomePage/HomePage";
import MovieDetails from "../../containers/MovieDetails/MovieDetails";

function Layout() {
    return (
        <div>
            <div>
                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/movieDetails/:id" exact component={MovieDetails}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </div>
        </div>
    )
}


export default Layout;
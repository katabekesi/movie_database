import React from 'react';
import "../../index.scss"
import { MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBCol } from 'mdbreact';



function MovieListItem(props) {

    return (
        <MDBCol className="card-long-list">
            <MDBCard className="image-container-long-list" style={{ width: "80rem" }}>
                <MDBCardImage className="img-fluid img-long-list img-carousel" src={props.imageUrl} movie />
                <MDBCardBody>
                    <MDBCardTitle className="movie-long-list-title">{props.title}</MDBCardTitle>
                    <MDBCardText className="movie-long-list-text">
                        {props.overview}
                    </MDBCardText>
                    <MDBBtn className="button button-green" href={"/movieDetails/" + props.id}>See more</MDBBtn>
                </MDBCardBody>
            </MDBCard>
        </MDBCol>

    )
}

export default MovieListItem;


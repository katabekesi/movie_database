import React from 'react';
import { MDBCard, MDBCardBody, MDBCardText, MDBCardTitle, MDBCol} from "mdbreact";
import "../../index.scss";
import StarRatingComponent from 'react-star-rating-component';


function CommentListItem(props) {
    return (

        <MDBCol className="card-long-list">
            <MDBCard className="image-container-long-list" style={{width: "40rem"}}>
                <MDBCardBody className="comment-card-body">
                    <div className="row">
                        <div className="col-md-2">
                            <ion-icon name="person"></ion-icon>
                        </div>
                        <div className="col-md-10">
                            <MDBCardTitle className="comment-author">{props.author}</MDBCardTitle>
                            <MDBCardText className="comment-creation-date">
                                {props.createdAt}
                            </MDBCardText>
                            <MDBCardText className="comment-details">
                               <StarRatingComponent
                                name="rate"
                                starCount={5}
                                value={props.rating}
                                starColor={"#28B483"}
                            />
                            </MDBCardText>
                            <MDBCardText className="comment-text">
                                {props.text}
                            </MDBCardText>
                        </div>
                    </div>
                </MDBCardBody>
            </MDBCard>
        </MDBCol>

    )

}

export default CommentListItem;

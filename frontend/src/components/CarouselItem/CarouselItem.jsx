import React from 'react';
import "../../index.scss"

function CarouselItem(props) {

    return (
        <div className="card-movie-list">
            <div className="image-container">
                <img className="img-carousel" src={props.imageUrl} alt="movie"/>
            </div>
        </div>
    )
}

export default CarouselItem;

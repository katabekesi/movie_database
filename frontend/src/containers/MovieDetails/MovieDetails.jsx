import React, {Component} from 'react';
import axios from "axios";
import {Link} from "react-router-dom";
import StarRatingComponent from 'react-star-rating-component';
import CommentListItem from "../../components/CommentListItem/CommentListItem";
import "../../index.scss";

class MovieDetails extends Component {
    BASE_URL = "https://api.themoviedb.org/3/movie/";
    API_KEY = "?api_key=84b2674dcba2f54328f0fef9d7abfe44";
    IMAGE_BASE_URL = "http://image.tmdb.org/t/p/";

    state = {
        details: [],
        mainProductionCompany: "",
        genres: [],
        cast: [],
        comments: [],
        commentForm: {
            author: "",
            text: "",
            rating: 0
        }

    };

    componentDidMount() {
        this.fetchData()

    }

    fetchData() {
        this.fetchDetailsData();
        this.fetchCreditData();
        this.fetchCommentData();
    }

    fetchDetailsData() {
        axios.get(this.BASE_URL + this.props.match.params.id + this.API_KEY)
            .then(response => {
                this.setState({
                    details: response.data,
                    mainProductionCompany: response.data.production_companies[0].name,
                    genres: response.data.genres
                })
            })
            .catch(err => console.warn(err))
    }

    fetchCreditData() {
        axios.get(this.BASE_URL + this.props.match.params.id + "/credits" + this.API_KEY)
            .then(response => {
                this.setState({
                    cast: response.data.cast,
                })
            })
            .catch(err => console.warn(err))
    }

    fetchCommentData() {
        axios.get("/api/comments/" + this.props.match.params.id)
            .then(response => {
                console.log("Response ", response);
                console.log("comments", this.state.comments)
                this.setState({comments: response.data});
            })
            .catch(console.warn);
    }

    inputChangedHandler = (event) => {
        const target = event.target;
        const updatedForm = {...this.state.commentForm};
        updatedForm[target.name] = target.value;
        this.setState({commentForm: updatedForm});
    };

    postDataHandler = (event) => {
        event.preventDefault();

        const formData = this.state.commentForm;
        let tempComments = this.state.comments;
        tempComments.push(formData)

        let url = '/api/comments/' + this.props.match.params.id;
        let method = 'post';

        axios({method: method, url: url, data: formData})
            .then(() => {
                this.setState({
                    comments: tempComments,
                    commentForm: {
                        author: "",
                        text: "",
                        rating: 0
                    }
                })
            })
            .catch(error => {
                console.warn(error);
            });
    };

    onStarClick = (nextValue) => {
        const updatedForm = {...this.state.commentForm};
        updatedForm.rating = nextValue;
        this.setState({commentForm: updatedForm})
    };


    render() {

        let genreList = [...this.state.genres];
        genreList.length = 2;
        const genres = genreList.map((genre) => {
            return (
                <li key={genre.id}>{genre.name}</li>
            )
        });

        let cast = [...this.state.cast];
        cast.length = 3;
        const castItems =
            cast.map((castItem) => {
                return (
                    <li key={castItem.id}>{castItem.name} - {castItem.character}</li>
                )
            });

        let releaseDate = new Date(this.state.details.release_date).getFullYear();


        const comments = this.state.comments.map((comment) =>
            <CommentListItem
                key={comment.id}
                text={comment.text}
                author={comment.author}
                rating={comment.rating}
                createdAt={comment.createdAt}
            />
        );

        return (
            <div>
                <section className="movie-details">
                    <div className="logo-box">
                        <Link className="logo" to="/">
                            <ion-icon name="film"></ion-icon>
                        </Link>
                    </div>
                    <div className="row">
                        <div className="col-md-2 details-company">
                            <h3>{this.state.mainProductionCompany}</h3>
                        </div>
                        <div className="col-10 details-box">
                            <h1 className="details-title">{this.state.details.title}</h1>
                            <span className="details-date"> ({releaseDate})</span>
                            <hr/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 details-image">
                            <img className="img-carousel"
                                 src={this.IMAGE_BASE_URL + "w300/" + this.state.details.poster_path} alt="movie"/>
                        </div>
                        <div className="col-md-6 details-misc ">
                            <div className="genres">
                                <ul>
                                    {genres}
                                </ul>
                            </div>
                            <div className="details-overview">
                                <p>{this.state.details.overview}</p>
                            </div>
                            <div className="cast-box">
                                <h5>Main Cast:</h5>
                                <ul>
                                    {castItems}
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="comment-section">
                        <div className="row comment-row">
                            {comments}
                        </div>

                    <form onSubmit={this.postDataHandler}>
                        <div className="form-group">
                            <label htmlFor="author">Username:</label>
                            <input
                                id="author"
                                type="text"
                                name="author"
                                value={this.state.commentForm.author}
                                onChange={this.inputChangedHandler}
                                placeholder="Username"
                                minLength="4"
                                maxLength="15"
                                className="form-control form-author"
                                required
                            />
                        </div>
                        <div className="form-group col-xs-4">
                            <label htmlFor="text">Comment:</label>
                            <textarea
                                id="text"
                                name="text"
                                value={this.state.commentForm.text}
                                onChange={this.inputChangedHandler}
                                placeholder="Enter your comment"
                                cols="30" rows="4"
                                className="form-control form-text"
                                required
                            >
                        </textarea>
                        </div>
                        <div>
                            <StarRatingComponent
                                name="rate"
                                starCount={5}
                                value={this.state.commentForm.rating}
                                onStarClick={this.onStarClick.bind(this)}
                                starColor={"#28B483"}
                            />
                        </div>
                        <button type="submit" className="btn button button-green">Add comment</button>
                    </form>
                </section>
            </div>
        )
    }

}

export default MovieDetails;
import React, {Component} from 'react';
import "../../index.scss";
import axios from "axios";
import CarouselItem from "../../components/CarouselItem/CarouselItem";
import Slider from "react-slick";
import MovieListItem from "../../components/MovieListItem/MovieListItem";
import {Link} from "react-router-dom";

class HomePage extends Component {
    BASE_URL = "https://api.themoviedb.org/";
    API_KEY = "?api_key=84b2674dcba2f54328f0fef9d7abfe44";

    state = {
        popularMovies: [],
        searchTitle: ""
    };

    componentDidMount() {
        for (let i = 1; i < 5; i++) {
            axios.get(this.BASE_URL + "3/movie/popular" + this.API_KEY + "&language=en-US&page=" + i)
                .then(response => {
                    let tempMovies = [...this.state.popularMovies];
                    tempMovies.push(...response.data.results);
                    this.setState({popularMovies: tempMovies})
                })
                .catch(err => console.warn(err))
        }
    }

    searchHandler = (event) => {
        this.setState({searchTitle: event.target.value});
    };




    render() {

        const settings = {
            dots: true,
            speed: 250,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
        };

        const imageBaseUrl = "http://image.tmdb.org/t/p/";
        const imageSizeCarousel = "w300/";
        const imageSizeList = "w200/";


        const carouselMovieList = [...this.state.popularMovies];
        carouselMovieList.length = 10;
        const popularMoviesToShow =
            carouselMovieList.map(movie => {
                return <CarouselItem
                    key={movie.id}
                    id={movie.id}
                    imageUrl={imageBaseUrl + imageSizeCarousel + movie.poster_path}
                />

            });


        let filteredList = this.state.popularMovies.filter(
            movie => {
                return movie.title.toLowerCase()
                    .indexOf(this.state.searchTitle.toLowerCase()) !== -1
            }
        );

        const popularMovieLongList =
            filteredList.map(movie => {
                return (
                    <li key={movie.id}>
                        <MovieListItem
                            id={movie.id}
                            title={movie.title}
                            imageUrl={imageBaseUrl + imageSizeList + movie.poster_path}
                            overview={movie.overview}
                        />
                    </li>
                )
            });








        return (
            <div>
                <header className="header">
                    <div className="logo-box">
                        <Link className="logo" to="/">
                            <ion-icon name="film"></ion-icon>
                        </Link>
                    </div>
                    <div className="text-box">
                        <h1 className="heading-primary">
                            <span className="heading-primary-main">Snapshot</span>
                            <span className="heading-primary-sub">Your Movie Database</span>
                        </h1>
                        <a href="#movie-list-id" className="button button-white">Browse Movies</a>
                    </div>
                </header>
                <main>
                    <section className="carousel">
                        <h2 className="heading-secondary">Popular movies you must see</h2>
                        <Slider {...settings}>
                            {popularMoviesToShow}
                        </Slider>
                    </section>
                    <section className="movie-list" id="movie-list-id">
                        <div className="container">
                            <div className="row">
                                <div className="col search-box">
                                    <label htmlFor="searchTitle" className="searchTitle-label">Search: </label>
                                    <input
                                        className="searchTitle"
                                        id="searchTitle"
                                        name="searchTitle"
                                        value={this.state.search}
                                        placeholder="Enter a title"
                                        maxlength="25"
                                        onChange={this.searchHandler}
                                    />
                                </div>
                            </div>
                            <div className="row justify-content-md-center">
                                <div className="col-md-auto">
                                    <ul className="movie-list-ul">
                                        {popularMovieLongList}
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </section>
                </main>
            </div>

        );
    }

}

export default HomePage;
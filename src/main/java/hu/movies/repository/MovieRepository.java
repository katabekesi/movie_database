package hu.movies.repository;

import hu.movies.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<Comment, Long> {

    Optional<List<Comment>> findCommentsByMovieIdOrderByCreatedAtDesc(Long id);

}

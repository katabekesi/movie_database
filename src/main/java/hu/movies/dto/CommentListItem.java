package hu.movies.dto;

import hu.movies.domain.Comment;

import java.time.format.DateTimeFormatter;

public class CommentListItem {

    private Long id;

    private String author;

    private String text;

    private Integer rating;

    private String createdAt;

    public CommentListItem() {
    }

    public CommentListItem(Comment comment) {
        this.id = comment.getId();
        this.author = comment.getAuthor();
        this.text = comment.getText();
        this.createdAt = comment.getCreatedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        this.rating = comment.getRating();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}

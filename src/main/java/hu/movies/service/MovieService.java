package hu.movies.service;

import hu.movies.domain.Comment;
import hu.movies.dto.CommentCreationCommand;
import hu.movies.dto.CommentListItem;
import hu.movies.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class MovieService {

    private MovieRepository movieRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public Comment createComment(CommentCreationCommand commentCreationCommand, Long id) {

        Comment comment = new Comment();
        comment.setAuthor(commentCreationCommand.getAuthor());
        comment.setText(commentCreationCommand.getText());
        comment.setCreatedAt(LocalDateTime.now());
        comment.setRating(commentCreationCommand.getRating());
        comment.setMovieId(id);

        return movieRepository.save(comment);
    }


    public List<CommentListItem> getCommentsByMovieId(Long id) {
        Optional<List<Comment>> optionalCommentsForMovie = movieRepository.findCommentsByMovieIdOrderByCreatedAtDesc(id);
        List<Comment> comments;
        List<CommentListItem> commentListItems = new ArrayList<>();
        if (optionalCommentsForMovie.isPresent()) {
            comments = optionalCommentsForMovie.get();
            for (Comment comment : comments) {
                commentListItems.add(new CommentListItem(comment));
            }
        }
        return commentListItems;
    }
}

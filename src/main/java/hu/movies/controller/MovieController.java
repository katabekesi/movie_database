package hu.movies.controller;

import hu.movies.dto.CommentCreationCommand;
import hu.movies.dto.CommentListItem;
import hu.movies.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class MovieController {

    private MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @PostMapping("/{id}")
    public ResponseEntity createCommentToMovie(@PathVariable Long id, @RequestBody CommentCreationCommand commentCreationCommand ) {
        movieService.createComment(commentCreationCommand, id);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<CommentListItem>> getCommentsByMovieId(@PathVariable Long id) {
        return new ResponseEntity<>(movieService.getCommentsByMovieId(id), HttpStatus.OK);
    }

}